<?php

namespace Amco\Kernel;

use Pimple\ServiceProviderInterface;
use Slim\Container;
use Slim\App as BaseApp;

/**
 * Class App
 * @package Amco\Kernel
 */
class App extends BaseApp
{
    /**
     * Register new services on dependency container
     */
    public function registerServices()
    {
        /**
         * @var $container Container
         */
        $container = $this->getContainer();

        $services = $container->settings['services'];

        if (is_array($services) && !empty($services)) {
            foreach ($services as $service) {
                /**
                 * @var $instance ServiceProviderInterface
                 */
                $instance = new $service();

                $container->register($instance);

                unset($instance);
            }
        }

        unset($container, $services, $service);
    }

    /**
     * Register App Middlewares
     */
    public function registerAppMiddlewares()
    {
        /**
         * @var $container Container
         */
        $container = $this->getContainer();

        $middlewares = $container->settings['middlewares'];

        if (is_array($middlewares) && !empty($middlewares)) {
            foreach ($middlewares as $middleware) {
                $this->add($middleware);
            }
        }

        unset($container, $middlewares, $middleware);
    }

    /**
     * Load and register routes by platform and version
     */
    public function registerRoutes()
    {
        /**
         * @var $container Container
         */
        $container      = $this->getContainer();
        $app            = $this;

        $api_version    = ($container->request->getQueryParams()['api_version'])
                          ?$container->request->getQueryParams()['api_version']
                          :env('DEFAULT_API_VERSION');
        $routesFile     = base_path()
                            .'/routes/'
                            .$container->settings['platform']
                            .'/'
                            .$api_version
                            .'/routes.php';

        $routesFileDefault   = base_path()
                                .'/routes/'
                                .$container->settings['platform']
                                .'/'
                                .env('DEFAULT_API_VERSION')
                                .'/routes.php';

        if (file_exists($routesFile)) {
            require_once $routesFile;
        } else if (file_exists($routesFileDefault)) {
            require_once $routesFileDefault;
        }

    }
}
