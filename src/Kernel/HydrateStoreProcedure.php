<?php

namespace Amco\Kernel;

use Doctrine\ORM\Internal\Hydration\AbstractHydrator;
use PDO;

class HydrateStoreProcedure extends AbstractHydrator
{
    /**
     * Hydrates all rows from the current statement instance at once.
     *
     * @return array
     */
    protected function hydrateAllData()
    {
        return $this->_stmt->fetchAll(PDO::FETCH_ASSOC);
    }
}