<?php

namespace Amco\Kernel\Couch;

use \Couchbase\Bucket;
use Doctrine\Common\Cache\CacheProvider;

class CouchbaseDriverCache extends CacheProvider
{
    /**
     * @var Bucket|null
     */
    private $couchbase;

    /**
     * Sets the Couchbase instance to use.
     *
     * @param Bucket $couchbase
     *
     * @return void
     */
    public function setCouchbase(Bucket $couchbase)
    {
        $this->couchbase = $couchbase;
    }

    /**
     * Gets the Couchbase instance used by the cache.
     *
     * @return Bucket|null
     */
    public function getCouchbase()
    {
        return $this->couchbase;
    }

    /**
     * {@inheritdoc}
     */
    protected function doFetch($id)
    {
        try {
            return $this->couchbase->get($id)->value;
        } catch (\Couchbase\Exception $ex) {
            return false;
        }
    }

    /**
     * {@inheritdoc}
     */
    protected function doContains($id)
    {
        try {
            $document = $this->couchbase->get($id);
            return true;
        } catch (\Couchbase\Exception $ex) {
            return false;
        }
    }

    /**
     * {@inheritdoc}
     */
    protected function doSave($id, $data, $lifeTime = 0)
    {
        if ($lifeTime > 30 * 24 * 3600) {
            $lifeTime = time() + $lifeTime;
        }
        try {
            return $this->couchbase->upsert($id, $data, ['expiry'=>(int)$lifeTime]);
        } catch (\Couchbase\Exception $ex) {
            return false;
        }
    }

    /**
     * {@inheritdoc}
     */
    protected function doDelete($id)
    {
        try {
            return $this->couchbase->remove($id);
        } catch (\Couchbase\Exception $ex) {
            return false;
        }
    }

    /**
     * {@inheritdoc}
     */
    protected function doFlush()
    {
        return false;
    }

    /**
     * {@inheritdoc}
     */
    protected function doGetStats()
    {
        return [];
    }
}