<?php

namespace Amco\Kernel;

use Doctrine\Common\EventManager;
use Doctrine\DBAL\Configuration;
use Doctrine\DBAL\Connection;

class EntityManager
{
    /**
     * The used Configuration.
     *
     * @var \Doctrine\DBAL\Configuration
     */
    private $config;

    /**
     * The database connection used by the EntityManager.
     *
     * @var \Doctrine\DBAL\Connection
     */
    private $conn;

    public function __construct(Connection $conn, Configuration $config, EventManager $eventManager)
    {
        $this->conn              = $conn;
        $this->config            = $config;
        $this->eventManager      = $eventManager;
    }
}