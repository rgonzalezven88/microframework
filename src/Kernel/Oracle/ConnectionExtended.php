<?php

namespace Amco\Kernel\Oracle;

use Doctrine\DBAL\Cache\ArrayStatement;
use Doctrine\DBAL\Cache\CacheException;
use Doctrine\DBAL\Cache\QueryCacheProfile;
use Doctrine\DBAL\Cache\ResultCacheStatement;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Statement;

class ConnectionExtended extends Connection
{
    /**
     * @param string $procedure
     * @param array $params
     * @param string $cursorname
     * @param QueryCacheProfile|null $qcp
     * @return mixed
     */
    public function execproc(string $procedure, array $params, string $cursorname = 'OUT_CUR', QueryCacheProfile $qcp = null)
    {
        if ($qcp !== null) {
            return $this->execprocCacheQuery($procedure, $params, $cursorname, $qcp);
        }

        $sql     = $this->createSql($procedure, $params, $cursorname);

        $stmt    = $this->prepare($sql);

        $this->bindParams($stmt, $params);

        $cursor = $this->_conn->getNewCursor();

        $stmt->getWrappedStatement()->bindCursor($cursorname, $cursor);

        $stmt->execute();

        $statement  = $this->prepare($cursor);

        $statement->execute();

        return $statement;
    }

    /**
     * @param string $procedure
     * @param array $params
     * @param string $cursorname
     * @param QueryCacheProfile|null $qcp
     * @return ArrayStatement|ResultCacheStatement
     * @throws CacheException
     */
    private function execprocCacheQuery(string $procedure, array $params, string $cursorname, QueryCacheProfile $qcp = null)
    {
        $resultCache = $qcp->getResultCacheDriver() ?: $this->_config->getResultCacheImpl();
        if ( ! $resultCache) {
            throw CacheException::noResultDriverConfigured();
        }

        list($cacheKey, $realKey) = $qcp->generateCacheKeys($procedure, $params, null);

        // fetch the row pointers entry
        if ($data = json_decode(json_encode($resultCache->fetch($cacheKey)),true)) {
            // is the real key part of this row pointers map or is the cache only pointing to other cache keys?
            if (isset($data[$realKey])) {
                $stmt = new ArrayStatement($data[$realKey]);
            } elseif (array_key_exists($realKey, $data)) {
                $stmt = new ArrayStatement(array());
            }
        }
        if (!isset($stmt)) {
            $stmt = new ResultCacheStatement($this->execproc($procedure, $params, $cursorname, null), $resultCache, $cacheKey, $realKey, $qcp->getLifetime());
        }

        $stmt->setFetchMode($this->defaultFetchMode);

        return $stmt;
    }

    /**
     * Create and format the sql
     *
     * @param string $procedure
     * @param array $params
     * @return string
     */
    private function createSql(string $procedure, array $params, $cursorname)
    {
        $sql  = 'begin ';
        $sql .= "dbms_application_info.set_module(module_name => '".str_replace('\'', '', $_SERVER['HTTP_HOST'])."', action_name => '$procedure'); ";
        $sql .= "$procedure(";
        if (is_array($params)) {
            foreach ($params as $k => $v) {
                if (!isset($v['VALUE']) || (isset($v['VALUE']) && is_null($v['VALUE'])) and (!isset($v['TYPE']) or $v['TYPE'] != 'CURSOR')) {
                    $sql .= 'NULL,';
                } else {
                    $sql .= ":{$v['NAME']},";
                }
            }
            $sql = substr($sql, 0, -1).',:'.$cursorname.'); end;';
        }
        return $sql;
    }

    /**
     * Bind all parameters
     *
     * @param Statement $stmt
     * @param array $params
     */
    private function bindParams(Statement $stmt, array $params)
    {
        if (is_array($params)) {
            foreach ($params as $param) {
                if (!isset($param['TYPE']) && $param['TYPE'] != 'CURSOR') {
                    $stmt->bindParam($param['NAME'], $param['VALUE'], \PDO::PARAM_STMT);
                } else {
                    $stmt->bindParam($param['NAME'], $param['VALUE']);
                }
            }
        }
    }
}