<?php

namespace Amco\Kernel\Oracle;

use Doctrine\DBAL\Driver\OCI8\OCI8Connection;
use Doctrine\DBAL\Driver\OCI8\OCI8Statement;

class OciOracleStatement extends OCI8Statement
{
    /**
     * Creates a new OCI8Statement that uses the given connection handle and SQL statement.
     *
     * @param resource                                  $dbh       The connection handle.
     * @param string                                    $statement The SQL statement.
     * @param \Doctrine\DBAL\Driver\OCI8\OCI8Connection $conn
     */
    public function __construct($dbh, $statement, OCI8Connection $conn)
    {
        if (is_string($statement)){
            parent::__construct($dbh, $statement, $conn);
        } else {
            $this->_sth = $statement;
            $this->_dbh = $dbh;
            $this->_conn = $conn;
        }
    }

    /**
     * Holds references to bound parameter values.
     *
     * This is a new requirement for PHP7's oci8 extension that prevents bound values from being garbage collected.
     *
     * @var array
     */
    private $boundValues = array();

    /**
     * @return resource
     */
    public function getNewCursor()
    {
        return oci_new_cursor($this->_dbh);
    }

    /**
     * @param string $cursorname
     * @param $cursor
     * @return bool
     */
    public function bindCursor($cursorname, &$cursor)
    {
        $this->boundValues[$cursorname] =& $cursor;
        return oci_bind_by_name($this->_sth, $cursorname, $cursor, -1, OCI_B_CURSOR);
    }

    /**
     * {@inheritdoc}
     */
    public function bindParam($column, &$variable, $type = null, $length = null)
    {
        $column = isset($this->_paramMap[$column]) ? $this->_paramMap[$column] : $column;

        if ($type == \PDO::PARAM_LOB) {
            $lob = oci_new_descriptor($this->_dbh, OCI_D_LOB);
            $lob->writeTemporary($variable, OCI_TEMP_BLOB);

            $this->boundValues[$column] =& $lob;

            return oci_bind_by_name($this->_sth, $column, $lob, -1, OCI_B_BLOB);
        } elseif ($length !== null) {
            $this->boundValues[$column] =& $variable;

            return oci_bind_by_name($this->_sth, $column, $variable, $length);
        }

        $this->boundValues[$column] =& $variable;

        return oci_bind_by_name($this->_sth, $column, $variable);
    }
}