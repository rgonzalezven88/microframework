<?php

namespace Amco\Kernel\Oracle;

use Doctrine\DBAL\Driver\OCI8\OCI8Connection;

class OciOracleConnection extends OCI8Connection
{
    /**
     * Create cursor for stored procedure
     *
     * @return mixed New statement handle, or FALSE on error.
     */
    public function getNewCursor()
    {
        return oci_new_cursor($this->dbh);
    }

    /**
     * @param mixed $statement
     * @return OciOracleStatement
     */
    public function prepare($statement)
    {
        return new OciOracleStatement($this->dbh, $statement, $this);
    }
}