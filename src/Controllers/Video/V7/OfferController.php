<?php

namespace Amco\Controllers\Video\V7;

use Amco\Kernel\ControllerAbstract;
use Amco\Kernel\Oracle\ConnectionExtended;
use Doctrine\DBAL\Cache\QueryCacheProfile;
use Slim\Http\Request;
use Slim\Http\Response;

/**
 * Class OfferController
 * @package Amco\Controllers\Video\V7
 */
class OfferController extends ControllerAbstract
{
    /**
     * Index Action
     *
     * @param Request $request
     * @param Response $response
     * @return string
     */
    public function index($request, $response)
    {
        /**
         * @var ConnectionExtended $conn
         */
        $conn       = $this->container['dbs']['plataforma_sl'];
        $results    = $conn->execproc(
                    'PACK_VTC_V9.PR_TARIFARIO', [
                            ['NAME' => 'P_REGION', 'VALUE' => 'colombia'],
                        ],'OUT_CUR', new QueryCacheProfile(60)
                    );
        $offers = $results->fetchAll();
        $results->closeCursor();
        return $response->withJson($offers,200);
    }
}
