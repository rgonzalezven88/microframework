<?php

namespace Amco\Services;

use Monolog\Logger;
use Monolog\Processor\UidProcessor;
use Monolog\Handler\StreamHandler;
use Pimple\Container;
use Pimple\ServiceProviderInterface;

/**
 * Class MonologServiceProvider
 * @package Amco\Services
 */
class MonologServiceProvider implements ServiceProviderInterface
{
    /**
     * Registers services on the given container.
     *
     * This method should only be used to configure services and parameters.
     * It should not get services.
     *
     * @param Container $pimple A container instance
     */
    public function register(Container $pimple)
    {
        $pimple['logger'] = function ($pimple) {

            $settings = $pimple->settings['logger'];

            $logger = new Logger($settings['name']);

            $logger->pushProcessor(new UidProcessor());

            $logger->pushHandler(new StreamHandler($settings['path'], Logger::DEBUG));

            unset($pimple, $settings);

            return $logger;
        };
    }
}
