<?php

namespace Amco\Services;

use Amco\Kernel\ErrorHandler;
use Pimple\Container;
use Pimple\ServiceProviderInterface;

/**
 * Class ErrorHandlerServiceProvider
 * @package Amco\Services
 */
class ErrorHandlerServiceProvider implements ServiceProviderInterface
{
    /**
     * Registers services on the given container.
     *
     * This method should only be used to configure services and parameters.
     * It should not get services.
     *
     * @param Container $pimple A container instance
     */
    public function register(Container $pimple)
    {
        $pimple['errorHandler'] = function ($pimple) {
            return new ErrorHandler($pimple['logger']);
        };
    }
}
