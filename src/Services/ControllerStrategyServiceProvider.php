<?php

namespace Amco\Services;

use Amco\Kernel\ControllerStrategy;
use Pimple\Container;
use Pimple\ServiceProviderInterface;

/**
 * Class ControllerStrategyServiceProvider
 * @package Amco\Services
 */
class ControllerStrategyServiceProvider implements ServiceProviderInterface
{
    /**
     * Registers services on the given container.
     *
     * This method should only be used to configure services and parameters.
     * It should not get services.
     *
     * @param Container $pimple A container instance
     */
    public function register(Container $pimple)
    {
        $pimple['foundHandler'] = function () {
            return new ControllerStrategy();
        };
    }
}
