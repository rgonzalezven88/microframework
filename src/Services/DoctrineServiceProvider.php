<?php

namespace Amco\Services;

use Amco\Kernel\Couch\CouchbaseDriverCache;
use Doctrine\Common\Cache\ApcuCache;
use Doctrine\Common\Cache\ArrayCache;
use Doctrine\Common\Cache\FilesystemCache;
use Doctrine\Common\Cache\MemcacheCache;
use Doctrine\Common\Cache\MemcachedCache;
use Doctrine\Common\Cache\RedisCache;
use Doctrine\Common\Cache\XcacheCache;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Tools\Setup;
use Pimple\Container;
use Doctrine\DBAL\DriverManager;
use Pimple\ServiceProviderInterface;

/**
 * Class DoctrineServiceProvider
 * @package Amco\Services
 */
class DoctrineServiceProvider implements ServiceProviderInterface
{
    /**
     * Registers services on the given container.
     *
     * This method should only be used to configure services and parameters.
     * It should not get services.
     *
     * @param Container $pimple A container instance
     */
    public function register(Container $pimple)
    {
        // Connection Manager
        $pimple['dbs'] = function ($pimple) {
            $dbs = new Container();
            foreach ($pimple['settings']['database']['connections'] as $name => $options) {
                $dbs[$name] = function ($dbs) use ($options, $pimple) {
                    $cache  = $pimple['orm.cache.factory']($options['cache'], $pimple['settings']['cache'][$options['cache']]);
                    $conn   = DriverManager::getConnection($options);
                    $conn->getConfiguration()->setResultCacheImpl($cache);
                    return $conn;
                };
            }
            return $dbs;
        };

        // shortcuts for the "default" DB
        $pimple['db'] = function ($pimple) {
            $dbs = $pimple['dbs'];
            return $dbs[$pimple['settings']['database']['default']];
        };

        // Cache Factories

        // Memcache
        $pimple['orm.cache.factory.backing_memcache'] = $pimple->protect(function () {
            return new \Memcache;
        });
        $pimple['orm.cache.factory.memcache'] = $pimple->protect(function ($cacheOptions) use ($pimple) {
            if (empty($cacheOptions['host']) || empty($cacheOptions['port'])) {
                throw new \RuntimeException('Host and port options need to be specified for memcache cache');
            }
            /** @var \Memcache $memcache */
            $memcache = $pimple['orm.cache.factory.backing_memcache']();
            $memcache->connect($cacheOptions['host'], $cacheOptions['port']);
            $cache = new MemcacheCache;
            $cache->setMemcache($memcache);
            return $cache;
        });

        // Memcached
        $pimple['orm.cache.factory.backing_memcached'] = $pimple->protect(function () {
            return new \Memcached;
        });
        $pimple['orm.cache.factory.memcached'] = $pimple->protect(function ($cacheOptions) use ($pimple) {
            if (empty($cacheOptions['host']) || empty($cacheOptions['port'])) {
                throw new \RuntimeException('Host and port options need to be specified for memcached cache');
            }
            /** @var \Memcached $memcached */
            $memcached = $pimple['orm.cache.factory.backing_memcached']();
            $memcached->addServer($cacheOptions['host'], $cacheOptions['port']);
            $cache = new MemcachedCache;
            $cache->setMemcached($memcached);
            return $cache;
        });

        // Redis
        $pimple['orm.cache.factory.backing_redis'] = $pimple->protect(function () {
            return new \Redis;
        });
        $pimple['orm.cache.factory.redis'] = $pimple->protect(function ($cacheOptions) use ($pimple) {
            if (empty($cacheOptions['host']) || empty($cacheOptions['port'])) {
                throw new \RuntimeException('Host and port options need to be specified for redis cache');
            }
            /** @var \Redis $redis */
            $redis = $pimple['orm.cache.factory.backing_redis']();
            $redis->connect($cacheOptions['host'], $cacheOptions['port']);
            if (isset($cacheOptions['password'])) {
                $redis->auth($cacheOptions['password']);
            }
            $cache = new RedisCache;
            $cache->setRedis($redis);
            return $cache;
        });

        // ArrayCache
        $pimple['orm.cache.factory.array'] = $pimple->protect(function () {
            return new ArrayCache;
        });

        // ApcuCache
        $pimple['orm.cache.factory.apc'] = $pimple->protect(function () {
            return new ApcuCache;
        });

        // XCache
        $pimple['orm.cache.factory.xcache'] = $pimple->protect(function () {
            return new XcacheCache;
        });

        // FilesystemCache
        $pimple['orm.cache.factory.filesystem'] = $pimple->protect(function ($cacheOptions) {
            if (empty($cacheOptions['path'])) {
                throw new \RuntimeException('FilesystemCache path not defined');
            }
            $cacheOptions += array(
                'extension' => FilesystemCache::EXTENSION,
                'umask' => 0002,
            );
            return new FilesystemCache($cacheOptions['path'], $cacheOptions['extension'], $cacheOptions['umask']);
        });

        // CouchDb
        $pimple['orm.cache.factory.couchbase'] = $pimple->protect(function($cacheOptions){
            $couchbase  = new \CouchbaseCluster($cacheOptions['host']);
            $bucket     = $couchbase->openBucket($cacheOptions['bucket'],$cacheOptions['password']);
            $cache = new CouchbaseDriverCache();
            $cache->setCouchbase($bucket);
            return $cache;
        });
        $pimple['orm.cache.factory'] = $pimple->protect(function ($driver, $cacheOptions) use ($pimple) {
            $cacheFactoryKey = 'orm.cache.factory.'.$driver;
            if (!isset($pimple[$cacheFactoryKey])) {
                throw new \RuntimeException("Factory '$cacheFactoryKey' for cache type '$driver' not defined (is it spelled correctly?)");
            }
            return $pimple[$cacheFactoryKey]($cacheOptions);
        });

        // Entity Manager
        $pimple['ems'] = function ($pimple) {
            $ems = new Container();
            foreach ($pimple['settings']['database']['connections'] as $name => $options) {
                $ems[$name] = function ($ems) use ($options, $pimple, $name) {
                    $cache = $pimple['orm.cache.factory']($options['cache'], $pimple['settings']['cache'][$options['cache']]);
                    $config = Setup::createAnnotationMetadataConfiguration([app_path().'/Entities'], false,null, $cache, true);
                    $em = EntityManager::create(
                        $pimple['dbs'][$name],
                        $config,
                        null // @TODO: Implement Event Manager
                    );
                    $em->getConfiguration()->addCustomHydrationMode('HydrateStoreProcedure', 'Amco\Kernel\HydrateStoreProcedure');
                    return $em;
                };
            }
            return $ems;
        };
    }
}