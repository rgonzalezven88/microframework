<?php

use Amco\Kernel\App;
use Dotenv\Dotenv;

/**
 * Load config enviroment from .env
 *
 * @link https://github.com/vlucas/phpdotenv
 */
if (file_exists(base_path() . '/.env')) {
    $_dotenv = new Dotenv(base_path());

    $_dotenv->load();

    unset($_dotenv);
}

/**
 * Init the APP
 *
 * @TODO: Implement PHP-DI
 */
$app = new App(['settings' => require config_path() . '/app.php']);

/**
 * Register Services without bootstrap file, using ServiceProviderInterface from Pimple
 *
 * @link https://pimple.symfony.com/#extending-a-container
 */
$app->registerServices();

/**
 * Register middleware
 *
 */
$app->registerAppMiddlewares();

/**
 * Load app routes
 */
$app->registerRoutes();

/**
 * Total ignition
 */
$app->run();