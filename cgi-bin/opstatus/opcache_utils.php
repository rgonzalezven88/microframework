<?
##!/usr/local/php/5.2.5/bin/php
#phpinfo();

/**
 *
 * Invalidate>
 * http://telmexmexicodes.esdc.tv/scripts/opcache_utils.php?token=i&debug=1&query=/inetpub/plataformas/esdc/oracle/v9/esdc/cgi-bin
 *
 * Invalidate file>
 * http://telmexmexicodes.esdc.tv/scripts/opcache_utils.php?token=f&debug=1&query=/inetpub/plataformas/esdc/oracle/v9/esdc/cgi-bin/index.php
 * 
 * Reset>
 * http://telmexmexicodes.esdc.tv/scripts/opcache_utils.php?token=r&debug=1
 *
 * Status>
 * http://telmexmexicodes.esdc.tv/scripts/opcache_utils.php?token=s&scripts=true
 * 
 * 
 */

date_default_timezone_set('America/Buenos_Aires'); // php nuevos

function handleError($errno, $errstr, $errfile, $errline, array $errcontext){ //manejo de errores por log
	//consoleln("ERROR :: [$errno, $errstr, $errfile, $errline] \r\n".print_r($errcontext,true));
	consoleln("ERROR :: [$errno, $errstr, $errfile, $errline]");
	die;
    //throw new ErrorException($errstr, 0, $errno, $errfile, $errline);
}
set_error_handler('handleError');

/**
 * MAIN
 */
list($token,$parameters,$debug) = get_parameters_get($_GET);
$opcu=new opcUtilities($debug);
$opcu->go($token,$parameters);
exit;

/**
 * FUNCIONES DE PARAMETROS
 */
function get_parameters_argv($pars){ //para llamado por cli
	$res=array('-n',array());
	for($i=1; $i<count($pars); $i++){
		$par=$pars[$i];
		if($par[0]=='-') $res[0]=substr($par,1); //es el comando
		else $res[1][]=$par; //agrego parametro
	}
	return($res);
}
function get_parameters_get($pars){ //para llamado por web
	$res=array('n',array(),false);
	foreach($pars as $k=>$v){
		if($k=='token') $res[0]=$v; //es el comando
		elseif($k=='debug') $res[2]=($v!='false' && $v!='0');
		else $res[1][$k]=$v; //agrego parametro
	}
	//var_dump($res);
	return($res);
}

/**
 * FUNCIONES DE LOG
 */
function consoleln($msg){ echo date("Y-m-d H:i:s")." | $msg \r\n"; }

/**
 * Clase de utilidades
 */
class opcUtilities {

	public $debug=false;

	function __construct($debug){
		$this->debug=($debug!=NULL);
	}

	public function go($token,$parameters){ // hub de comandos
		for($k=0;$k<strlen($token);$k++){ //multicomando
			$comm=$token[$k];
			switch($comm){
				case 'f' : // invalidate
					$query=($parameters['query'])?$parameters['query']:$parameters[0];
					if($query=='') $query='/';
					consoleln("Ejecutando f [invalidate-files] >> query $query");
					consoleln($this->invalidate_file($query)." files invalidated. [RESULT=OK]");
					break;
				case 'i' : // invalidate
					$query=($parameters['query'])?$parameters['query']:$parameters[0];
					if($query=='') $query='/';
					consoleln("Ejecutando i [invalidate] >> query $query");
					consoleln($this->invalidate($query)." files invalidated. [RESULT=OK]");
					break;
				case 'r' : // reset
					consoleln("Ejecutando r [reset] >> void");
					$this->reset();
					break;
				case 's' : // status
					$scripts=($parameters['scripts'])?$parameters['scripts']:$parameters[0];
					consoleln("Ejecutando s [status] >> scripts $scripts");
					$this->status($scripts);
					consoleln("-----  [RESULT=OK]");
					break;
				case 'c' : // config
					consoleln("Ejecutando c [config] >> void");
					$this->config();
					consoleln("-----  [RESULT=OK]");
					break;
				default	:
					consoleln("ERROR | Unrecognized Command | $comm");
			}
		}
	}

	public function invalidate($path){ // invalida un query recursivamente
		$files=opcache_get_status(true); 
		$files=$files['scripts'];
		$largo=strlen($path);
		$cant=0;
		foreach($files as $k=>$v){
			$q=$v["full_path"];
			if(substr($q,0,$largo)==$path){ 
				opcache_invalidate($q,TRUE); $cant++;
				if($this->debug) consoleln("invalidated $q");
			}
		}
		return($cant);
	}

	public function invalidate_file($path){ // invalida un query recursivamente
		opcache_invalidate($path,TRUE);
		return 1;
	}

	public function reset(){ // resetea todo el cache
		opcache_reset();
	}

	public function status($full){ // estado actua (con scripts opcional) a log
		consoleln(print_r(opcache_get_status($full),true));
	}

	public function config(){ // configuracion a log
		consoleln(print_r(opcache_get_configuration(),true));
	}
}
