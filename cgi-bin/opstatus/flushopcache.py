from multiprocessing import Pool
import urllib2

import sys

flush_uri_prefix="http://localhost/services/opstatus/opcache_utils.php?token=f&debug=1&query=/inetpub/services/"

def opcache_clean(http_file_list):
	urls = parse_file_list(http_file_list)
	pool = Pool(processes = 10)
	pool.map(async_task, urls)


def async_task(url):
	req = urllib2.Request(flush_uri_prefix + url)
	req.add_header('Host', 'microfwk.clarovideo.net')

	r = urllib2.urlopen(req)
	if r.getcode() != 200:
		raise Exception("Status code: " + str(r.getcode()))

def parse_file_list(http_file_list):
	req = urllib2.Request(http_file_list)
	r = urllib2.urlopen(req)

	if r.getcode() != 200:
		raise Exception("Status code: " + str(r.getcode()))

	return r.read().split()

if __name__ == '__main__':
	if len(sys.argv) != 2:
		print "Usage python opcache.py <http_file_list>"
		exit(1)

	opcache_clean(sys.argv[1])
