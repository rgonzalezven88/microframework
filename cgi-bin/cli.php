<?php
/**
 * Sirve para ejecutar apis desde el CLI
 *
 * Forma de uso:
 * php cli.php <environment> <ruta del api> <querystring>
 * Ejemplo: php cli.php 'telmexmexico_dev' '/feed/appleumcavailability' 'api_version=v5&authpn=webclient&authpt=tfg1h3j4k6fd7&device_category=mobile&device_manufacturer=generic&device_model=android&device_type=web'
 */

namespace Routes;

$env = $argv[1];
$api = $argv[2];
$querystring = $argv[3];

// En algunos lugares toman los parametros directo del request
parse_str($querystring, $params);
foreach ($params as $key => $value) {
	$_REQUEST[$key] = $value;
	$_GET[$key] = $value;
	$_POST[$key] = $value;
}

//--Definiciones desde Apache/Nginx------------------
define('__SCRIPTS_ROOT__','/inetpub/services/cgi-bin/');
define('__TARGET_ROOT__','/inetpub/services/target/'.$env.'/');
define('__LOG_ROOT__','/inetpub/services/log/');

ini_set('error_reporting', E_ALL & ~E_NOTICE & ~E_STRICT);

/**
 * Autoloader
 */
require_once(__SCRIPTS_ROOT__.'../vendor/autoload.php');

$env = \Slim\Environment::mock([
    'REQUEST_METHOD' => 'GET',
    'REQUEST_URI' => $api,
    'PATH_INFO' => $api,
    'QUERY_STRING' => $querystring,
    'SERVER_NAME' => 'microfwk.clarovideo.net',
]);

require __SCRIPTS_ROOT__ . '/../src/app.php';

$time_start = microtime(true);

$app = \Slim\Slim::getInstance();
preDispatch($app);
$response = $app->run();

$app->logger->info('Total Execution Time: '.round(microtime(true) - $time_start, 4).' Sec');