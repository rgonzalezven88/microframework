<?php

// Newrelic
if (extension_loaded ('newrelic')) {
    newrelic_name_transaction(explode("?",$_SERVER['REQUEST_URI'])[0]);
}

// PHP-CLI
if (PHP_SAPI == 'cli-server') {
    // To help the built-in PHP dev server, check if the request was actually for
    // something which should probably be served as a static file
    $url  = parse_url($_SERVER['REQUEST_URI']);
    $file = __DIR__ . $url['path'];
    if (is_file($file)) {
        return false;
    }
}

// Vendor autoload
require __DIR__ . '/../vendor/autoload.php';

// Ignition
require __DIR__ . '/../bootstrap/app.php';