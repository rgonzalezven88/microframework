<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Main Configuration
    |--------------------------------------------------------------------------
    |
    | Here you may specify the different configurations on the App
    |
    */

    'determineRouteBeforeAppMiddleware' => false,

    'displayErrorDetails' => env('APP_DEBUG', false),

    'platform'      => env('APP_PLATFORM', 'video'),

    'enviroment'    => env('APP_ENV', 'development'),

    'middlewares'   => require 'middlewares.php',

    'services'      => require 'services.php',

    'logger'        => require 'logger.php',

    'database'      => require 'database.php',

    'cache'         => require 'cache.php',
];
