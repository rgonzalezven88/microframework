<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Autoloaded Services
    |--------------------------------------------------------------------------
    |
    | The service providers listed here will be automatically loaded on the
    | request to your application. Feel free to add your own services to
    | this array to grant expanded functionality to your applications.
    |
    */

    \Amco\Services\MonologServiceProvider::class,
    \Amco\Services\ErrorHandlerServiceProvider::class,
    \Amco\Services\ControllerStrategyServiceProvider::class,
    \Amco\Services\DoctrineServiceProvider::class,
];
