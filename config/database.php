<?php

use Amco\Kernel\Oracle\ConnectionExtended;
use Amco\Kernel\Oracle\OracleDriver;

return [

    /*
    |--------------------------------------------------------------------------
    | Default Database Connection Name
    |--------------------------------------------------------------------------
    |
    | Here you may specify which of the database connections below you wish
    | to use as your default connection for all database work. Of course
    | you may use many connections at once using the Database library.
    |
    */

    'default' => env('DEFAULT_CONNECTION', 'plataforma_sl'),

    /*
    |--------------------------------------------------------------------------
    | Database Connections
    |--------------------------------------------------------------------------
    |
    | Here are each of the database connections setup for your application.
    |
    */

    'connections' => [

        'plataforma' => [
            'driverClass'   => OracleDriver::class,
            'wrapperClass'  => ConnectionExtended::class,
            'host'          => env('PLATAFORMA_HOST', 'dbcvtest.local'),
            'dbname'        => env('PLATAFORMA_DBNAME', 'cvtst'),
            'user'          => env('PLATAFORMA_USER', 'web_esdc81'),
            'password'      => env('PLATAFORMA_PASS', 'frolixx8'),
            'charset'       => env('PLATAFORMA_CHARSET', 'AL32UTF8'),
            'cache'         => env('PLATAFORMA_CACHE', 'array'),
        ],

        'plataforma_sl' => [
            'driverClass'   => OracleDriver::class,
            'wrapperClass'  => ConnectionExtended::class,
            'host'          => env('PLATAFORMA_SL_HOST', 'dbcvtest.local'),
            'dbname'        => env('PLATAFORMA_SL_DBNAME', 'cvtst'),
            'user'          => env('PLATAFORMA_SL_USER', 'web_esdc81'),
            'password'      => env('PLATAFORMA_SL_PASS', 'frolixx8'),
            'charset'       => env('PLATAFORMA_SL_CHARSET', 'AL32UTF8'),
            'cache'         => env('PLATAFORMA_SL_CACHE', 'array'),
        ],

    ],

 ];
