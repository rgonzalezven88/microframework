<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Cache Connections
    |--------------------------------------------------------------------------
    |
    | Here are each of the cache connections setup for your application.
    |
    */

    'default'   => 'couchbase',
    'couchbase'   => [
        'host'          => env('COUCHDB_HOST', '10.10.16.234'),
        'user'          => env('COUCHDB_USER', 'Adminsitrator'),
        'password'      => env('COUCHDB_PASSWORD', 'arch33tx'),
        'bucket'        => env('COUCHDB_BUCKET', 'default'),
    ],

];