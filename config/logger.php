<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Logging Configuration
    |--------------------------------------------------------------------------
    |
    | Here you may configure the log settings for your application.
    | the Monolog PHP logging library. This gives
    | you a variety of powe1rful log handlers / formatters to utilize.
    |
    |
    */

    'name' => 'Aplication',
    'path' => log_path() . '/app.log',

];