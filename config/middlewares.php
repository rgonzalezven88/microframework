<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Autoloaded Middlewares
    |--------------------------------------------------------------------------
    |
    | The Middlewares listed here will be automatically loaded on the
    | request to your application. Feel free to add your own middlewares to
    | this array to grant expanded functionality to your applications.
    |
    */

    //\Amco\Middlewares\AuthMiddleware::class,
];
